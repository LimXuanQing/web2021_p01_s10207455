﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Web_s10207455.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace Web_s10207455.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public async Task<ActionResult> StudentLogin()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(
            OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                HttpContext.Session.SetString("LoginID", userName + " / "
                + eMail);
                HttpContext.Session.SetString("Role", "Student");
                HttpContext.Session.SetString("LoggedInTime",
                DateTime.Now.ToString());
                return RedirectToAction("Index", "Book");
            }
            catch (Exception e)
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }

        [HttpPost]
         public ActionResult StaffLogin(IFormCollection formData)
         { 
             // Read inputs from textboxes
             // Email address converted to lowercase
             string loginID = formData["txtLoginID"].ToString().ToLower();
             string password = formData["txtPassword"].ToString();

             if (loginID == "abc@npbook.com" && password == "pass1234")
             {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Staff");
                HttpContext.Session.SetString("DateTime", DateTime.Now.ToString("dd MMMM yyyy HH:mm:ss tt"));

                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("StaffMain");
             }
             else
             {
                // Store an error message in TempData for display at the index view
                TempData["Message"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
             }
         }
         public ActionResult StaffMain()
         {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
         }

        public async Task<ActionResult> LogOut()
        {
            //Compute login duration
            DateTime startTime = Convert.ToDateTime(HttpContext.Session.GetString("DateTime"));
            DateTime endTime = DateTime.Now;
            TimeSpan loginDuration = endTime - startTime;

            //Format display of login duration
            string strLoginDuration = "";
            if(loginDuration.Days > 0)
            {
                strLoginDuration += loginDuration.Days.ToString() + " day(s) ";
            }
            if (loginDuration.Hours > 0)
            {
                strLoginDuration += loginDuration.Hours.ToString() + " hour(s) ";
            }
            if (loginDuration.Minutes > 0)
            {
                strLoginDuration += loginDuration.Minutes.ToString() + " minute(s) ";
            }
            if (loginDuration.Seconds > 0)
            {
                strLoginDuration += loginDuration.Seconds.ToString() + " second(s) ";
            }

            TempData["LoginDuration"] = "You have logged in for " + strLoginDuration;

            // Clear authentication cookie
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);

            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
