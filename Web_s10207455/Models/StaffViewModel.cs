﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web_s10207455.Models
{
    public class StaffViewModel
    {
        [Display(Name = "ID")]
        public int StaffId { get; set; }
        public string Name { get; set; }
        public char Gender { get; set; }
        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }
        public string Nationality { get; set; }
        [Display(Name = "Email Address")]
        public string Email { get; set; }
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal Salary { get; set; }
        public string Status { get; set; }
        [Display(Name = "Branch")]
        public string BranchName { get; set; }
        public string Photo { get; set; }

        public IFormFile fileToUpload { get; set; }
    }
}
