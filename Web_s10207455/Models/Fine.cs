﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Web_s10207455.Models
{
    public class Fine
    {
        [Display(Name = "Due Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DueDate { get; set; }
        [Display(Name = "Number of Books Overdue")]
        [Range(1, 10, ErrorMessage =
        "Invalid value! Please enter a value from 1 to 10")]
        public int NumBooksOverdue { get; set; }
        // To do . . .
        // Define the following 3 properties with the display annotation
        // NumDaysOverDue: int; display name: "Number of Days Overdue"
        // FineRate: double; display name: "Fine Rate (SGD)"; display format:
        // 2 decimal places and comma at the thousand mark;
        // ApplyFormatInEditMode = true
        // FineAmt: double; display name: "Fine (SGD)";
        // display format: same as FineRate

        [Display(Name = "Number of Days Overdue")]
        public int NumDaysOverdue { get; set; }

        [Display(Name = "Fine Rate (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}", ApplyFormatInEditMode = true)]
        [Range(0, 99, ErrorMessage =
        "Invalid value! Please enter a value from 0 to 99")]
        public double FineRate { get; set; }

        [Display(Name = "Fine (SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public double FineAmt { get; set; }
    }
}
