﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_s10207455.Models
{
    public class BranchViewModel
    {
        public List<Branch> branchList { get; set; }
        public List<Staff> staffList { get; set; }
        public BranchViewModel()
        {
            branchList = new List<Branch>();
            staffList = new List<Staff>();
        }
    }
}
