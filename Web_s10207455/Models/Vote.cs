﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_s10207455.Models
{
    public class Vote
    {
        public int BookId { get; set; }
        public string Justification { get; set; }
    }
}
